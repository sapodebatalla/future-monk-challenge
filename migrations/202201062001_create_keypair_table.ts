import { QueryInterface, Sequelize } from "sequelize";
import { DataType } from "sequelize-typescript";
import { Migration } from "../src/app";

export const up: Migration = async ({ context: queryInterface }) => {
  return queryInterface.createTable("keypairs", {
    keypairId: {
      type: DataType.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement:true
    },
    key: {
      type: DataType.STRING(255),
      allowNull: false,
      unique: true
    },
    value: {
      type: DataType.STRING(3000),
    },
    createdAt: {
      type: DataType.DATE,
      allowNull: false,
    },
    updatedAt: {
      type: DataType.DATE,
      allowNull: false,
    },
  });
};

export const down: Migration = async ({ context: queryInterface }) => {
  return queryInterface.dropTable("keypairs");
};
