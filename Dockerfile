FROM node:14 as base

RUN mkdir /usr/src/app
WORKDIR /usr/src/app

ENV PATH /usr/src/app/node_modules/.bin:$PATH

COPY package.json /usr/src/app/package.json
COPY package-lock.json /usr/src/app/package-lock.json

RUN npm install

RUN npm install nodemon -g

COPY . /usr/src/app/

EXPOSE 3000

FROM base as production

ENV NODE_PATH=./build

RUN npm run clean
RUN npm run build