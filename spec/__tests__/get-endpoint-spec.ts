import * as request from 'supertest';
import app from '../../src/app';


describe('GET endpoint', function () {
  it('responds with 404', function(done){
    request
      .agent(app)
      .get('/api/keypairs/keypair/2')
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(404, done);
  });
});
