# Future Monk Challenge

## Description

This is a dev test I was tasked to do for a hiring process.

### What we would like you to build

Build a service which provides both a websocket server and a REST endpoint.
The websocket server should accept messages containing any key-value pair and store it.
The REST endpoint should accept GET requests specifying a key and should return the
corresponding value for that key, or an appropriate HTTP error if the key does not exist.
Key-value state should persist between executions of the service (in other words, if the service
shuts down and restarts for some reason, key-value pairs that were set before the restart should
be restored). Multiple simultaneous websocket connections should be possible and should be
allowed to change the state, without concurrency issues.
Feel free to add more features to show off your skills!

### What we want to receive

- A zip or public git repository with the relevant files for the project
- Instructions on how to get it up and running locally
- Additional thoughts/comments if any

### What we will be looking at

- Clean code
- Tests
- General usage/best practices

## Installation

In order to run this project you will need Docker: https://docs.docker.com/engine/install/ubuntu/
You will also need docker-compose: https://docs.docker.com/compose/install/

## Usage

After cloning the repository.

- docker-compose build
- docker-compose up
  This should start the server listening on port 3000.
  Also, it starts a postgreSQL instance listening on port 2345.
  The file client.html is a socket.io client implementation.

You can consume the /api/keypairs/keypair/:key endpoint with a GET request to see the corresponding value.

## Support

If you need any help with running or understanding this code, you can send me an email pedro.polo531(at)gmail(dot)com

## Project status

This is a dev test that will most likely be abandoned once it's fullfilled its purpouse.
