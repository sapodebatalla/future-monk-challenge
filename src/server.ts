import app from './app';
import { createServer } from 'http';
import { Server } from 'socket.io';
import { initSocketServer } from './socket-server';
export const httpServer = createServer(app);
const io = new Server(httpServer,{cors:{
  origin: '*',
  methods:['GET','POST']
}});
initSocketServer(io);
const server = httpServer.listen(app.get('port'), () => {
  console.log(
    '  App is running at http://localhost:%d in %s mode',
    app.get('port'),
    app.get('env')
  );
  console.log('  Press CTRL-C to stop\n');
});
