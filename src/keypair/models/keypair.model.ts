import { AutoIncrement, Column, DataType, Model, PrimaryKey, Table, Unique } from "sequelize-typescript";

export interface IKeyPair{
    keypairId?: number;
    key: string;
    value: string;
}

@Table({tableName: 'keypairs'})
export class KeyPair extends Model implements IKeyPair{

    @AutoIncrement
    @PrimaryKey
    @Column(DataType.BIGINT)
    keypairId!: number;

    @Unique
    @Column
    key!: string;

    @Column
    value!: string;
}