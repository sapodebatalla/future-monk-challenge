import { IKeyPair, KeyPair } from "../models/keypair.model";

export const saveKeyPair = async (keypair: IKeyPair): Promise<KeyPair> => {
  try {
    const nKeyPair = new KeyPair(keypair);
    return nKeyPair.save();
  } catch (e) {
    return Promise.reject(e);
  }
};

export const saveOrUpdate = async (keypair: IKeyPair): Promise<KeyPair> => {
  try {
    let nKeyPair: KeyPair | null = await getKeyPair(keypair.key);
    if (nKeyPair) return nKeyPair.update(keypair);
    nKeyPair = new KeyPair(keypair);
    return nKeyPair.save();
  } catch (e) {
    return Promise.reject(e);
  }
};

export const getKeyPair = async (key: string) => {
  return KeyPair.findOne({ where: { key: key } });
};

export const getKeyPairs = async () => {
  return KeyPair.findAll();
};
