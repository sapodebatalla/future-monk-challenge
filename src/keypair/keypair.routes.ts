import { Router } from "express";
import * as keyPairController from "./controllers/keypair.controller";

const router: Router =  Router();

router.get('/keypair/:key',[], keyPairController.getKeyPair)

export default router;