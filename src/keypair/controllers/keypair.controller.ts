import { Response, Request } from "express";
import { KeyPair } from "../models/keypair.model";
import * as keypairService from "../services/keypair.service";

export const getKeyPair = async (req: Request, res: Response) => {
  try {
    const key: string = req.params.key;
    if (!key) return res.status(404).send({ message: "No results found" });
    keypairService.getKeyPair(key).then((val: KeyPair | null) => {
      if (!val) return res.status(404).send({ message: "No results Found" });
      return res.status(200).send(val);
    });
  } catch (e) {
    return res.status(500).send(e);
  }
};
