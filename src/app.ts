// Basic imports
import express, { Router } from "express";
import cors from "cors";
import morgan from "morgan";
import helmet from "helmet";
import { Sequelize } from "sequelize-typescript";
import { testDbConnection } from "./database/init/init_database";
import { defineModels } from "./database/model-definitions";
import { QueryInterface } from "sequelize/dist";
import { SequelizeStorage, Umzug } from "umzug";

// Router
import keyPairRouter from './keypair/keypair.routes';

// Connect to database.
const sequelize = new Sequelize({
  dialect: "postgres",
  database: process.env.DATABASE_NAME,
  username: process.env.DATABASE_USER || "postgres",
  password: process.env.DATABASE_PASS || "postgres",
  host: process.env.DATABASE_URL || "localhost",
  port: Number(process.env.DATABASE_PORT) || 5432,
});
//Check if database exists & create
try {
  testDbConnection(sequelize).then(() => {
    sequelize
      .getQueryInterface()
      .showAllSchemas()
      .then((val: any) => {
        console.log(val);
      });
  });
} catch (e) {
  console.log(e);
}

// I would've done this outside of the app.
// However, I don't want to orchestrate too much stuff.
try {
  console.log("Sequelize created");
  if (process.env.PRODUCTION === "true") {
    var umzug: Umzug<QueryInterface> = new Umzug({
      migrations: { glob: "build/migrations/*.js" },
      context: sequelize.getQueryInterface(),
      storage: new SequelizeStorage({ sequelize }),
      logger: console,
    });
  } else {
    var umzug: Umzug<QueryInterface> = new Umzug({
      migrations: { glob: "migrations/*.ts" },
      context: sequelize.getQueryInterface(),
      storage: new SequelizeStorage({ sequelize }),
      logger: console,
    });
  }
 (async () => {
    console.log("Umzug up");
    await umzug.up();
  })().then(() => {
    testDbConnection(sequelize).then(() => {
      // Define Models
      defineModels(sequelize);
    });
  });
} catch (e) {
  console.log(e);
}
// Little hack for Umzug migrations
export type Migration = typeof umzug._types.migration;
// Define the app
const app = express();
app.set("port", process.env.PORT || 3000);
app.use(cors({ origin: "*", credentials: true }));
app.use(helmet());
app.use(morgan("tiny"));

const router: Router = Router();
// # Endpoints

router.use('/keypairs',keyPairRouter)

app.use("/api", router);


export default app;
