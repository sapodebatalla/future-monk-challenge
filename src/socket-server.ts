import { Server } from 'socket.io';
import * as keypairService from './keypair/services/keypair.service';

export const initSocketServer = (io: Server) => {
  io.on('connection', (socket) => {
    console.log(`${socket.id} Connected`);

    socket.on(
      'send-values',
      async (key: string, value: string, overwrite: boolean, callback) => {
        console.log(key, value, overwrite);
        try {
          if (overwrite) {
            await keypairService.saveOrUpdate({ key, value });
            callback({ status: 0, message: 'Saved successfully' });
          } else {
            await keypairService.saveKeyPair({
              key: key.toString(),
              value: value.toString(),
            });
            callback({ status: 0, message: 'Saved successfully' });
          }
        } catch (e) {
          console.log(e);
          callback({ status: 1, message: 'Error', error: e });
        }
      }
    );
  });
};
