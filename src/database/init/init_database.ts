import { Sequelize } from "sequelize-typescript";

export const testDbConnection = async (sequelize: Sequelize) => {
    try {
      await sequelize.authenticate();
      console.log('DB Authenticated');
    } catch (error) {
      console.log(error);
      process.exit(1);
    }
  }