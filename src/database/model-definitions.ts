import { Sequelize } from "sequelize-typescript";
import {KeyPair} from "../keypair/models/keypair.model";

export const defineModels = (seq: Sequelize) => {
  console.log("Defining Models");

  seq.addModels([KeyPair]);
  console.log("Done");
};
